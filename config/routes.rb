Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users
  root "home#index"

  namespace :api do
    namespace :v1 do
      get 'me' => 'credentials#me'
    end
  end

  get '/change_password' => 'home#edit_password'
  post '/change_password' => 'home#update_password'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
