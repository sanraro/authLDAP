class Api::V1::CredentialsController < Api::V1::ApiController
  before_action :doorkeeper_authorize!

  def me
    if user = (current_user || current_resource_owner)
      logger.info "========> [Api::V1::Credentials.me] Requested user: #{user.name}"
      attribs = user.attributes.to_hash.slice('id', 'name', 'email')
    else
      logger.warn "========> [Api::V1::Credentials.me] No logged user"
    end
    render json: attribs
  end

end
