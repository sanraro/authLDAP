class Api::V1::ApiController < ActionController::Base
  respond_to     :json

  protected

  # Get user
  def current_resource_owner
    if doorkeeper_token
      owner = User.find_by(id: doorkeeper_token.resource_owner_id)
      logger.info "========> [Api.V1.current_resource_owner] Identified user #{owner.inspect}"
      return owner
    end
  end
end
